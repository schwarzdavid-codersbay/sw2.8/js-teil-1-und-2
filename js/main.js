const names = [
    'Max',
    'Herbert',
    'Mia',
    'Albert'
]
console.log(names[0])

function logNames() {
    for (let i = 0; i < names.length; i++) {
        console.log(i, names[i])
    }
    console.log('Hi')
}
logNames()

document.querySelector('#addName').addEventListener('click', function () {
    const newName = document.querySelector('#nameInput').value
    names.push(newName)
    logNames()
})

const listItems = Array.from(document.querySelectorAll('li')) // 🤡
console.log(listItems)
for(let i = 0; i < listItems.length; i++) {
    listItems[i].addEventListener('click', function () {
        console.log(listItems[i].textContent)
    })
}

const attendees = []
const people = attendees

attendees.push({
    firstName: 'Hans',
    lastName: 'Doskozil',
    ratings: [5, 4, 3]
})

attendees.push({
    firstName: 'Seppi',
    lastName: 'Dogozil',
    ratings: [3, 4, 2]
})

console.log(people)

function calculateAttendeeAverageRating(attendee) {
    /*let sum = 0;
    for (let j = 0; j < attendee.ratings.length; j++) {
        sum += attendee.ratings[j]
    }*/
    const sum = attendee.ratings.reduce(function (output, rating) {
        return output + rating
    }, 0)

    attendee.firstName = attendee.firstName.toUpperCase()
    return sum / attendee.ratings.length;
}

function logAttendeeRatings(persons) {
    for (let attendee of persons) {
        console.log(attendee.firstName)
        const average = calculateAttendeeAverageRating(attendee);
        console.log(attendee.firstName)
        console.log(average)
    }
    persons.push({
        firstName: 'John',
        lastName: 'Doe',
        ratings: [4, 3, 5]
    })
}

logAttendeeRatings(attendees);
console.log(attendees)

for (let i in attendees) {
    console.log(i + ': ' + attendees[i].firstName)
    const average = calculateAttendeeAverageRating(attendees[i]);
    console.log(average)
}

for (let i = 0; i < Math.min(attendees.length, 50); i++) {
    console.log(attendees[i].firstName)
    const average = calculateAttendeeAverageRating(attendees[i]);
    console.log(average)
}

// Personen filtern

function filterArray(array, callback) {
    const filteredAttendees = []
    for (let attendee of array) {
        const keepPerson = callback(attendee)
        if (keepPerson) {
            filteredAttendees.push(attendee);
        }
    }
    return filteredAttendees
}

const filteredAttendees = filterArray(attendees, function(person) {
    return person.firstName.length > 4
})
const filteredAttendees2 = filterArray(people, function (person) {
    return calculateAttendeeAverageRating(person) > 2
})

const filteredAttendees3 = attendees.filter(function (person) {
    return person.firstName.length > 4
})
const sortedAttendees = attendees.sort(function (personA, personZ) {
    console.log(personA, personZ)
    return personA.firstName.toLowerCase().localeCompare(personZ.firstName.toLowerCase());
})
console.log(sortedAttendees)

/*const hans = attendees.filter(function (person) {
    return person.firstName === 'Hans'
})[0]*/
const hans = attendees.find(function (person) {
    return person.firstName === 'HANS'
})
const hansIndex = attendees.findIndex(function (person) {
    return person.firstName === 'HANS'
})
console.log(hansIndex, hans)
attendees.splice(hansIndex, 1)


//const names = ['Maria', 'Sarah', 'Leo', 'Herbert']
const sortedLowercaseNames = names.map(function (name) {
    return name.toLowerCase();
})
console.log(names, sortedLowercaseNames)







// Events und so..

document.querySelector('#nameInput').addEventListener('input', function () {
    console.log('Input')
})

for(let listItem of listItems) {
    const span = listItem.querySelector('span')

    let offsetX = 0;
    let offsetY = 0;

    listItem.addEventListener('dragstart', function (event) {
        console.log('dragstart', event)
        offsetX = event.offsetX
        offsetY = event.offsetY
        span.classList.add('drag')
    })

    listItem.addEventListener('drag', function (event) {
        console.log('drag', event)
        span.style.top = (event.pageY - offsetX) + 'px'
        span.style.left = (event.pageX - offsetY) + 'px'
    })

    listItem.addEventListener('dragend', function () {
        console.log('dragend')
        span.classList.remove('drag')
    })
}